import sys, pygame
from pygame.locals import *
import AI

pygame.init()

size = screenwidth, screenheight = 464, 464
tilesize = screenwidth/8	# assume size is square and gameboard takes entire screen
transparent = 228, 199, 176

screen = pygame.display.set_mode(size)

background_img = pygame.image.load("images/background.gif").convert()
backgroundrot_img = pygame.transform.rotate(background_img, 180)

bbishop_img = pygame.image.load("images/pieces/bbishop.gif").convert()
bking_img = pygame.image.load("images/pieces/bking.gif").convert()
bknight_img = pygame.image.load("images/pieces/bknight.gif").convert()
bpawn_img = pygame.image.load("images/pieces/bpawn.gif").convert()
bqueen_img = pygame.image.load("images/pieces/bqueen.gif").convert()
brook_img = pygame.image.load("images/pieces/brook.gif").convert()
wbishop_img = pygame.image.load("images/pieces/wbishop.gif").convert()
wking_img = pygame.image.load("images/pieces/wking.gif").convert()
wknight_img = pygame.image.load("images/pieces/wknight.gif").convert()
wpawn_img = pygame.image.load("images/pieces/wpawn.gif").convert()
wqueen_img = pygame.image.load("images/pieces/wqueen.gif").convert()
wrook_img = pygame.image.load("images/pieces/wrook.gif").convert()

bbishop_img.set_colorkey(transparent)
bking_img.set_colorkey(transparent)
bknight_img.set_colorkey(transparent)
bpawn_img.set_colorkey(transparent)
bqueen_img.set_colorkey(transparent)
brook_img.set_colorkey(transparent)
wbishop_img.set_colorkey(transparent)
wking_img.set_colorkey(transparent)
wknight_img.set_colorkey(transparent)
wpawn_img.set_colorkey(transparent)
wqueen_img.set_colorkey(transparent)
wrook_img.set_colorkey(transparent)

def snaptogrid(rect=None):	# warning: does not make a copy of rect!
	if rect is None:
		rect = pygame.Rect(0,0,0,0)
	rect.topleft = round(float(rect.topleft[0])/tilesize)*tilesize, \
			round(float(rect.topleft[1])/tilesize)*tilesize
	if rect.left < 0:
		rect.left = 0
	if rect.right > screenwidth:
		rect.right = screenwidth
	if rect.top < 0:
		rect.top = 0
	if rect.bottom > screenheight:
		rect.bottom = screenheight
	return rect
	
def rect2pos(rect):
	return rect.topleft[0]/tilesize+1, rect.topleft[1]/tilesize+1
	
def pos2rect(pos):
	return pygame.Rect((pos[0]-1)*tilesize, (pos[1]-1)*tilesize, tilesize, tilesize)
	
#def getcollided(p):	# the piece to test collisions with
#for i in pieces:
#	if p.rect.colliderect(i.rect) \
#			and i != p:
#		screen.blit(i.img, i.rect)

class Board:
	def __init__(self, pieces, background = pygame.Surface(size)):
		self.width, self.height = 8, 8
		self.playerturn = 1	# white
		self.history = []	# 0 if not pawn doublemove
		self.background = background
		self.rotated = 0
		self.pieces = pieces
		
	def isoccupied(self, pos):	# is a piece here?
		for p in self.pieces:
			if p.pos[0] == pos[0] and p.pos[1] == pos[1]:
				return 1
		return 0
		
	def getoccupied(self, pos):	# return the piece
		for p in self.pieces:
			if p.pos[0] == pos[0] and p.pos[1] == pos[1]:
				return p
		return None
		
	def isonboard(self, pos):
		if pos[0] < 1 or pos[0] > board.width \
				or pos[1] < 1 or pos[1] > board.height:
			return 0
		else:
			return 1
		
	#def remove(self, pos):	# remove the piece
	#	for p in self.pieces:
	#		if p.pos[0] == pos[0] and p.pos[1] == pos[1]:
	#			 self.pieces.remove(p)
	#			 return
	
	def add(self, piece):
		self.pieces.append(piece)
	
	def remove(self, piece):
		for p in self.pieces:
			if p == piece:
				self.pieces.remove(p)
				 
	def getking(self, color):	# assume only 2 colors
		if color == 0:
			return self.pieces[0]	# WARNING once removed, altered
		else:
			return self.pieces[1]
			
	def getoppking(self, color):	# assume only 2 colors
		if color == 1:
			return self.pieces[0]
		else:
			return self.pieces[1]
			
	def check(self, color):
		#if color == 0:
		#	print "black"
		#else:
		#	print "white"
		return
		
	def endturn(self, color):
		if color == 0:
			self.playerturn = 1
		else:
			self.playerturn = 0
			
	def rotate(self):
		for p in board.pieces:
			if isinstance(p, Pawn):
				p.direction *= -1
				
			p.pos = 9-p.pos[0], 9-p.pos[1]
			p.rect = pos2rect(p.pos)
				
		if self.rotated:
			self.background = backgroundrot_img
			self.rotated = 0
		else:
			self.background = background_img
			self.rotated = 1
			
class Piece:
	def __init__(self, x, y):	# x and y represent tiles; counting starts at 1
		self.pos = self.x, self.y = x, y	# down and right are positive
		self.namelock = 0	# boolean - whether or not currently dragging the piece
		self.color = -1
		
	def move(self, x, y, board):	# x and y represent tiles; counting starts at 1; doesn't move the piece graphically
		oldpos = self.pos
		newpos = self.pos[0]+x, self.pos[1]+y
		
		if board.playerturn != self.color \
				or x == 0 and y == 0 \
				or not self.check(board, newpos):
			self.rect = pos2rect(oldpos)
			return
		
		attacked = None
		
		if board.isoccupied(newpos):
			if board.getoccupied(newpos).color == self.color:
				self.rect = pos2rect(oldpos)
				return
			else:
				attacked = board.getoccupied(newpos)
				
		self.pos = newpos
		king = board.getking(self.color)
		
		for p in board.pieces:	# do tests on the new position
			if p == attacked:
				continue
			if p.color != self.color and p.check(board, king.pos):
				self.pos = oldpos
				self.rect = pos2rect(oldpos)
				return
		
		if attacked is not None:
			board.remove(attacked)
				
		#self.check(board, board.getotherking(self.color).pos)	# leads to checkmate
		
		board.history.append(0)
		board.endturn(self.color)

class Bishop(Piece):
	def __init__(self, x, y, color):	# 0 is black, not 0 is white
		Piece.__init__(self, x, y)
		
		self.color = color
		
		if color == 0:
			self.img = bbishop_img
			self.oppcolor = 1
		else:
			self.img = wbishop_img
			self.oppcolor = 0
			
		self.rect = self.img.get_rect().move((x-1)*tilesize, (y-1)*tilesize)	# move from (0, 0)
		
	def check(self, board, pos):	# are you in line of sight of pos?
		d = pos[0]-self.pos[0], pos[1]-self.pos[1]
		
		if abs(d[0]) != abs(d[1]):
			return 0
		
		m = abs(d[0])
		
		for i in range(m-1):
			testpos = self.pos[0]+d[0]/m*(i+1), self.pos[1]+d[1]/m*(i+1)
			if board.isoccupied(testpos):
				return 0
				
		return 1
		
class Rook(Piece):
	def __init__(self, x, y, color):	# 0 is black, not 0 is white
		Piece.__init__(self, x, y)
		
		self.hasmoved = 0
		self.color = color
		
		if color == 0:
			self.img = brook_img
			self.oppcolor = 1
		else:
			self.img = wrook_img
			self.oppcolor = 1
			
		self.rect = self.img.get_rect().move((x-1)*tilesize, (y-1)*tilesize)	# move from (0, 0)
	
	def move(self, x, y, board):	# x and y represent tiles; counting starts at 1; doesn't move the piece graphically
		Piece.move(self, x, y, board)
		self.hasmoved = 1
		
	def check(self, board, pos):	# are you in line of sight of pos?
		d = pos[0]-self.pos[0], pos[1]-self.pos[1]
		
		if d[0] != 0 and d[1] != 0:
			return 0
		
		m = max(abs(d[0]), abs(d[1]))
		
		for i in range(m-1):
			testpos = self.pos[0]+d[0]/m*(i+1), self.pos[1]+d[1]/m*(i+1)
			if board.isoccupied(testpos):
				return 0
				
		return 1

class Knight(Piece):
	def __init__(self, x, y, color):	# 0 is black, not 0 is white
		Piece.__init__(self, x, y)
		
		self.color = color
		
		if color == 0:
			self.img = bknight_img
			self.oppcolor = 1
		else:
			self.img = wknight_img
			self.oppcolor = 0
			
		self.rect = self.img.get_rect().move((x-1)*tilesize, (y-1)*tilesize)	# move from (0, 0)
		
	def check(self, board, pos):	# are you in attacking sight of pos?
		d = pos[0]-self.pos[0], pos[1]-self.pos[1]
		
		if not (abs(d[0]) == 1 and abs(d[1]) == 2) \
				and not (abs(d[0]) == 2 and abs(d[1]) == 1):
			return 0
		
		return 1
			
class Queen(Piece):
	def __init__(self, x, y, color):	# 0 is black, not 0 is white
		Piece.__init__(self, x, y)
		
		self.color = color
		
		if color == 0:
			self.img = bqueen_img
			self.oppcolor = 1
		else:
			self.img = wqueen_img
			self.oppcolor = 0
			
		self.rect = self.img.get_rect().move((x-1)*tilesize, (y-1)*tilesize)	# move from (0, 0)
		
	def check(self, board, pos):	# are you in line of sight of pos?
		d = pos[0]-self.pos[0], pos[1]-self.pos[1]
		
		if abs(d[0]) != abs(d[1]) \
				and (d[0] != 0 and d[1] != 0):
			return 0
		
		m = max(abs(d[0]), abs(d[1]))
		
		for i in range(m-1):
			testpos = self.pos[0]+d[0]/m*(i+1), self.pos[1]+d[1]/m*(i+1)
			if board.isoccupied(testpos):
				return 0
		
		return 1
			
class King(Piece):
	def __init__(self, x, y, color):	# 0 is black, not 0 is white
		Piece.__init__(self, x, y)
		
		self.hasmoved = 0
		self.color = color
		
		if color == 0:
			self.img = bking_img
			self.oppcolor = 1
		else:
			self.img = wking_img
			self.oppcolor = 0
			
		self.rect = self.img.get_rect().move((x-1)*tilesize, (y-1)*tilesize)	# move from (0, 0)
	
	def move(self, x, y, board):	# x and y represent tiles; counting starts at 1
		oldpos = self.pos
		newpos = self.pos[0]+x, self.pos[1]+y
		
		iscastle = abs(x) == 2 and y == 0 and not self.hasmoved
		
		if board.playerturn != self.color \
				or x == 0 and y == 0 \
				or not (abs(x) <= 1 and abs(y) <= 1) \
					and not iscastle:
			self.rect = pos2rect(oldpos)
			return
		
		attacked = None
				
		if not iscastle:
			if board.isoccupied(newpos):
				if board.getoccupied(newpos).color == self.color:
					self.rect = pos2rect(oldpos)
					return
				else:
					attacked = board.getoccupied(newpos)
				
			self.pos = newpos
			
			for p in board.pieces:	# do tests on the new position
				if p == attacked:
					continue
				if p.color != self.color and p.check(board, self.pos):
					self.pos = oldpos
					self.rect = pos2rect(oldpos)
					return
			
			if attacked is not None:
				board.remove(attacked)
			
			self.hasmoved = 1
			
			board.history.append(0)
			board.endturn(self.color)
			
		else:	# castle
			if self.color == 0 and not board.rotated:
				if x < 0: r = board.getoccupied((1, 1))
				else: r = board.getoccupied((8, 1))
			elif self.color == 1 and not board.rotated:
				if x < 0: r = board.getoccupied((1, 8))
				else: r = board.getoccupied((8, 8))
			elif self.color == 0 and board.rotated:
				if x < 0: r = board.getoccupied((1, 8))
				else: r = board.getoccupied((8, 8))
			else:	# self.color == 1 and board.rotated:
				if x < 0: r = board.getoccupied((1, 1))
				else: r = board.getoccupied((8, 1))
				
			if not isinstance(r, Rook) \
					or r.hasmoved \
					or not r.check(board, self.pos) \
					or not r in board.pieces:
				self.rect = pos2rect(oldpos)
				return
				
			d = r.pos[0]-oldpos[0]
				
			for i in range(3):
				testpos = oldpos[0]+d/abs(d)*i, oldpos[1]
				
				for p in board.pieces:	# do tests on the new position
					if p.color != self.color and p.check(board, testpos):
						self.rect = pos2rect(oldpos)
						return
			
			self.pos = newpos
			r.pos = self.pos[0]-d/abs(d), r.pos[1]	#move rook
			r.rect = pos2rect(r.pos)
			
			self.hasmoved = 1
			
			board.history.append(0)
			board.endturn(self.color)
		
	def check(self, board, pos):	# are you in attacking sight of pos?
		d = pos[0]-self.pos[0], pos[1]-self.pos[1]

		if not (abs(d[0]) <= 1 and abs(d[1]) <= 1):
			return 0
			
		return 1
			
class Pawn(Piece):
	def __init__(self, x, y, color):	# 0 is black, not 0 is white
		Piece.__init__(self, x, y)
		
		self.nmoves = 0	# number of moves taken
		self.color = color
		
		if color == 0:
			self.img = bpawn_img
			self.direction = 1	# down
			self.oppcolor = 1
		else:
			self.img = wpawn_img
			self.direction = -1	# up
			self.oppcolor = 0
			
		self.rect = self.img.get_rect().move((x-1)*tilesize, (y-1)*tilesize)	# move from (0, 0)
	
	def move(self, x, y, board):	# x and y represent tiles; counting starts at 1
		oldpos = self.pos
		newpos = self.pos[0]+x, self.pos[1]+y
		
		isdoublemove = x == 0 and y*self.direction == 2 and self.nmoves == 0
		iscapture = abs(x) == 1 and y*self.direction == 1
		
		if board.playerturn != self.color \
				or x == 0 and y == 0 \
				or not (x == 0 and y*self.direction == 1) \
					and not isdoublemove \
					and not iscapture:
			self.rect = pos2rect(oldpos)
			return
		
		attacked = None
		
		if isdoublemove:
			testpos = newpos[0], newpos[1]-self.direction
			if board.isoccupied(testpos):
				self.rect = pos2rect(self.pos)
				return
				
		if board.isoccupied(newpos):
			if board.getoccupied(newpos).color == self.color:
				self.rect = pos2rect(oldpos)
				return
			elif iscapture:
				attacked = board.getoccupied(newpos)
			else:	# an attempt to capture piece in front
				self.rect = pos2rect(oldpos)
				return
		else:	# newpos not occupied
			testpos = newpos[0], newpos[1]-self.direction
			p = None
			enpassant = 0
			
			if board.isoccupied(testpos):
				p = board.getoccupied(testpos)
				last = len(board.history)-1
				if isinstance(p, Pawn) \
						and p.nmoves == 1 \
						and p.color != self.color \
						and board.history[last] == 1:
					enpassant = 1
				
			if iscapture and not enpassant:
				self.rect = pos2rect(self.pos)
				return
			elif iscapture and enpassant:
				board.remove(p)
				
		self.pos = newpos
		king = board.getking(self.color)
		
		for p in board.pieces:	# do tests on the new position
			if p == attacked:
				continue
			if p.color != self.color and p.check(board, king.pos):
				self.pos = oldpos
				self.rect = pos2rect(oldpos)
				return
		
		if attacked is not None:
			board.remove(attacked)
			
		if self.direction*self.pos[1] == 8 \
				or self.direction*self.pos[1] == -1:
			self.rect = pos2rect((-1, -1))
			board.remove(self)
			q = Queen(self.pos[0], self.pos[1], self.color)
			board.add(q)	# other pieces are options
			q.rect = pos2rect(newpos)
				
		#self.check(board, board.getotherking(self.color).pos)	# leads to checkmate
		
		self.nmoves += 1
		
		if isdoublemove:
			board.history.append(1)
		else:
			board.history.append(0)
			
		board.endturn(self.color)
		
	def check(self, board, pos):	# are you in attacking sight of pos?
		d = pos[0]-self.pos[0], pos[1]-self.pos[1]
		
		if abs(d[0]) != 1 \
				or self.direction*d[1] != 1:
			return 0
		
		return 1
			
def redrawcollided(p):	# the piece to test collisions with
	for i in board.pieces:
		if p.rect.colliderect(i.rect) \
				and i != p:
			screen.blit(i.img, i.rect)
			
def redrawall():
	for i in board.pieces:
		screen.blit(i.img, i.rect)
				
bbishop = Bishop(3, 1, 0)
bbishop2 = Bishop(6, 1, 0)
wbishop = Bishop(3, 8, 1)
wbishop2 = Bishop(6, 8, 1)
brook = Rook(1, 1, 0)
brook2 = Rook(8, 1, 0)
wrook = Rook(1, 8, 1)
wrook2 = Rook(8, 8, 1)
bknight = Knight(2, 1, 0)
bknight2 = Knight(7, 1, 0)
wknight = Knight(2, 8, 1)
wknight2 = Knight(7, 8, 1)
bqueen = Queen(4, 1, 0)
wqueen = Queen(4, 8, 1)
bking = King(5, 1, 0)
wking = King(5, 8, 1)
bpawn1 = Pawn(1, 2, 0)
bpawn2 = Pawn(2, 2, 0)
bpawn3 = Pawn(3, 2, 0)
bpawn4 = Pawn(4, 2, 0)
bpawn5 = Pawn(5, 2, 0)
bpawn6 = Pawn(6, 2, 0)
bpawn7 = Pawn(7, 2, 0)
bpawn8 = Pawn(8, 2, 0)
wpawn1 = Pawn(1, 7, 1)
wpawn2 = Pawn(2, 7, 1)
wpawn3 = Pawn(3, 7, 1)
wpawn4 = Pawn(4, 7, 1)
wpawn5 = Pawn(5, 7, 1)
wpawn6 = Pawn(6, 7, 1)
wpawn7 = Pawn(7, 7, 1)
wpawn8 = Pawn(8, 7, 1)

pieces = [bking, wking, bbishop, bbishop2, wbishop, wbishop2, brook, brook2, \
			wrook, wrook2, bknight, bknight2, wknight, wknight2, bqueen, wqueen, \
			bpawn1, bpawn2, bpawn3, bpawn4, bpawn5, bpawn6, bpawn7, bpawn8, \
			wpawn1, wpawn2, wpawn3, wpawn4, wpawn5, wpawn6, wpawn7, wpawn8]	# warning: kings must be first
					
board = Board(pieces, background_img)

screen.blit(board.background, (0, 0))
for p in board.pieces:
	screen.blit(p.img, p.rect)

while 1:
	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			sys.exit()
			
		if pygame.key.get_pressed()[K_r] == 1:
			board.rotate()
			screen.blit(board.background, (0, 0))
			redrawall()
			
	for p in board.pieces:
		if pygame.mouse.get_pressed()[0] == 1 \
				and p.rect.collidepoint(pygame.mouse.get_pos()):
			counter = 0
			for i in board.pieces:	# make sure nothing is namelocked
				counter += i.namelock
			if counter == 0:
				p.namelock = 1
			
		if pygame.mouse.get_pressed()[0] == 1 \
				and p.namelock == 1:
			screen.blit(board.background, p.rect, p.rect)
			p.rect.center = pygame.mouse.get_pos()
			redrawall()
			screen.blit(p.img, p.rect)
			
		if pygame.mouse.get_pressed()[0] == 0 \
				and p.namelock == 1:
			p.namelock = 0
			
			screen.blit(board.background, p.rect, p.rect)
			redrawcollided(p)
			newpos = rect2pos(snaptogrid(p.rect))	# warning: snaptogrid moves p.rect
			p.move(newpos[0]-p.pos[0], newpos[1]-p.pos[1], board)
			if isinstance(p, Pawn) \
					or isinstance(p, King):
				screen.blit(board.background, (0, 0))	# in case of castle, pawn promotion, or en passant
				redrawall()
			screen.blit(board.background, p.rect, p.rect)	# in case of capture
			screen.blit(p.img, p.rect)	# rect is already in place if valid move

	pygame.display.update()
	pygame.time.delay(10)	# var
